from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter import PhotoImage
from tkinter.filedialog import *
from constant import *

class Vue:

	def __init__(self, parent):
		self.controleur = parent
		self.root = Tk()
		self.root.title("Quoi de neuf docteur?!")

		self.user = StringVar()
		self.password = StringVar()
		self.userIns = StringVar()
		self.passwordIns = StringVar()
		self.passwordInsVerif = StringVar()
		self.Sexe = "Homme"
		self.firstNameIns = StringVar()
		self.LastNameIns = StringVar()
		self.emailIns = StringVar()
		self.dateNaissance = StringVar()
		self.erreur = StringVar()
		self.barre = None
		self.menuButton = None
		

		self.createMainFrame()
		self.createSecondFrames()
		self.root.minsize(600,600)

	def initHeader(self):
		self.pageLabel = Label(self.topFrame,text="Connexion",background="#00f",padx=20,font=("Comic sans ms","40","bold"))
		self.pageLabel.pack(fill=BOTH,side=LEFT)
		self.pageLabel.pack_propagate(False)

	def createMainFrame(self):
		self.mainFrame = ttk.Frame(self.root, padding="3 3 3 3", height="600", width="600")
		self.mainFrame.pack(expand=1,fill=BOTH)
		self.mainFrame.pack_propagate(False)
		self.topFrame = Frame(self.mainFrame,background="#00f",height="80",width="474")
		self.topFrame.pack(fill=BOTH,side=TOP)
		self.topFrame.pack_propagate(False)
		self.barre = Frame(self.mainFrame,background="#000",height="4",width="474")
		self.barre.pack(fill=BOTH,side=TOP)
		self.barre.pack_propagate(False)
		self.initHeader()
		self.defaultImage = PhotoImage(file="user.ppm",width="80",height="80")
		self.userImage = self.defaultImage
		self.imageList = []
		

	def createSecondFrames(self):
		
		self.bottomFrame = Frame(self.mainFrame,background ="#FFF",height="600",width="474")
		self.bottomFrame.pack(fill=BOTH,side=TOP, expand=1)
		self.bottomFrame.pack_propagate(False)




	def headerContent(self):

		# --------------------------------------------------------------- #
		self.barre = Frame(self.topFrame,background="#00F",height="80",width="30")
		self.barre.pack(side=RIGHT)

		self.menuButton = Menubutton(self.topFrame,image=self.userImage,bd=3,text="aaaaa",padx=15, width="80",height="80",relief=RAISED, background="white")
		self.menuButton.pack(side=RIGHT)
		self.menuButton.pack_propagate(False)

		self.menuButton.menu = Menu(self.menuButton, tearoff = 0)
		self.menuButton["menu"] = self.menuButton.menu
		self.imageDisconnect = PhotoImage(file="dc.ppm",width="80",height="80")
		self.imageHome = PhotoImage(file="home.ppm",width="80",height="80")
		self.imageProfile = PhotoImage(file="profile.ppm",width="80",height="80")
		self.imageFriends = PhotoImage(file="friends.ppm",width="80",height="80")
		self.menuButton.menu.add_command(image=self.imageHome, background="white", command=lambda:self.changePage("Accueil"))
		self.menuButton.menu.add_command(image=self.imageProfile, background="white", command=lambda:self.changePage("Profil",self.controleur.getUsername()))
		self.menuButton.menu.add_command(image=self.imageFriends, background="white", command=lambda:self.changePage("Amis"))
		self.menuButton.menu.add_command(image=self.imageDisconnect, background="white", command=lambda:self.changePage("Connexion"))
		

		#self.reload(self.width)

	def showConnectionPage(self):
		

		# --------------------------------------------------------------- #

		Frame(self.bottomFrame,height="10",width="350").pack(side=TOP)
		self.ConnectionFrame = Frame(self.bottomFrame,height="400",width="350")
		self.ConnectionFrame.pack(side=TOP)
		self.ConnectionFrame.pack_propagate(False)

		self.buttonConnectFrame= Frame(self.bottomFrame,height="50",width="550")
		self.buttonConnectFrame.pack(side=TOP)

		#-- Nom d'utilisateur --#

		l = Label(self.ConnectionFrame,text="Nom d'utilisateur:",height=1,width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=0 ,columnspan= 3,rowspan=2, pady=20, sticky=(W,S))
		e = Entry(self.ConnectionFrame,font=("Arial","18"), width=15, textvariable=self.user)
		e.grid(column=3,row=0 ,columnspan= 3,rowspan=2, pady=20, sticky=(N,W))
		e.focus()
		#-- Mot de passe --#

		l = Label(self.ConnectionFrame,text="Mot de passe:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=2 ,columnspan= 3,rowspan=2, pady=20, sticky=(N,W))
		e = Entry(self.ConnectionFrame,font=("Arial","18"),show="*", width=15, textvariable=self.password)
		e.grid(column=3,row=2 ,columnspan= 3,rowspan=2, pady=20, sticky=(N,W))

		#-- Bouton --#
		
		b = Button(self.ConnectionFrame,font=("Comic sans ms","18","bold"), text="Inscription",width=12, height=1,command=lambda:self.changePage("Inscription"))
		b.grid(column=0,row=4 ,columnspan= 3,rowspan=2, pady=20, sticky=(N,W))
		l = LabelFrame(self.ConnectionFrame,text=" ",width=15,font=("Comic sans ms","16","bold"))
		l.grid(column=3,row=4 ,columnspan= 1,rowspan=2, pady=20, sticky=(N,W))
		b = Button(self.ConnectionFrame,font=("Comic sans ms","18","bold"), text="Connect",width=8, height=1, command=lambda:self.controleur.connect(self.user.get(),self.password.get()))
		b.grid(column=4,row=4 ,columnspan= 2,rowspan=2, pady=20, sticky=(N,W))
		
	def showInscriptionPage(self):

		# --------------------------------------------------------------- #

		Frame(self.bottomFrame,height="10",width="350").pack(side=TOP)
		self.InscriptionFrame = Frame(self.bottomFrame,height="600",width="350")
		self.InscriptionFrame.pack(side=TOP)
		self.InscriptionFrame.pack_propagate(False)

		self.buttonConnectFrame= Frame(self.bottomFrame,height="50",width="550")
		self.buttonConnectFrame.pack(side=TOP)
		

		#-- Nom d'utilisateur --#

		l = Label(self.InscriptionFrame,text="Nom d'utilisateur:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=0 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"), width=15, textvariable=self.userIns)
		e.grid(column=3,row=0 ,columnspan= 3, pady=8, sticky=(N,W))
		e.focus()
		#-- Mot de passe --#

		l = Label(self.InscriptionFrame,text="Mot de passe:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=1 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"),show="*", width=15, textvariable=self.passwordIns)
		e.grid(column=3,row=1 ,columnspan= 3, pady=8, sticky=(N,W))

		#-- Prénom --#
		
		l = Label(self.InscriptionFrame,text="Prénom",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=2 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"), width=15, textvariable=self.firstNameIns)
		e.grid(column=3,row=2 ,columnspan= 3, pady=8, sticky=(N,W))

		#-- Nom --#

		l = Label(self.InscriptionFrame,text="Nom:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=3 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"), width=15, textvariable=self.LastNameIns)
		e.grid(column=3,row=3 ,columnspan= 3, pady=8, sticky=(N,W))

		#-- Email --#

		l = Label(self.InscriptionFrame,text="Courriel:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=4 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"), width=15, textvariable=self.emailIns)
		e.grid(column=3,row=4 ,columnspan= 3, pady=8, sticky=(N,W))
		
		#-- Date de naissance --#
		l = Label(self.InscriptionFrame,text="Date de naissance:",width=15,anchor=W,font=("Comic sans ms","16","bold"))
		l.grid(column=0,row=5 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.InscriptionFrame,font=("Arial","18"), width=15, textvariable=self.dateNaissance)
		e.grid(column=3,row=5 ,columnspan= 3, pady=8, sticky=(N,W))
		
		#-- Sexe --#
		self.sexeHomme = Checkbutton(self.InscriptionFrame,text="Homme", command=lambda:self.changeSexe("Homme"))
		self.sexeHomme.grid(column=3,row=6 ,columnspan= 1, pady=8, sticky=(N,W))
		self.sexeHomme.select()
		self.sexeFemme = Checkbutton(self.InscriptionFrame,text="Femme", command=lambda:self.changeSexe("Femme"))
		self.sexeFemme.grid(column=4,row=6 ,columnspan= 1, pady=8, sticky=(N,W))
		self.sexeFemme.deselect()
		self.sexeAutre = Checkbutton(self.InscriptionFrame,text="Autre", command=lambda:self.changeSexe("Autre"))
		self.sexeAutre.grid(column=5,row=6 ,columnspan= 1, pady=8, sticky=(N,W))
		self.sexeAutre.deselect()
		
		#-- Bouton --#
		b = Button(self.InscriptionFrame,font=("Comic sans ms","18","bold"), text="Accueil",width=12, height=1,command=lambda:self.changePage("Connexion"))
		b.grid(column=0,row=7 ,columnspan= 3,rowspan=2, pady=20, sticky=(N,W))
		l = LabelFrame(self.InscriptionFrame,text=" ",width=15,font=("Comic sans ms","16","bold"))
		l.grid(column=3,row=7 ,columnspan= 1,rowspan=2, pady=20, sticky=(N,W))
		b = Button(self.InscriptionFrame,font=("Comic sans ms","18","bold"), text="Confirmer",width=8, height=1, command=lambda:self.controleur.register(self.userIns.get(),self.passwordIns.get(),self.firstNameIns.get(),self.LastNameIns.get(),self.emailIns.get(),self.dateNaissance.get(),self.Sexe))
		b.grid(column=4,row=7 ,columnspan= 2,rowspan=2, pady=20, sticky=(N,W))

	def showModifPage(self, infoUser):
		# --------------------------------------------------------------- #

		self.headerContent()

		Frame(self.bottomFrame,height="10",width="450").pack(side=TOP)
		self.ModifFrame = Frame(self.bottomFrame,height="550",width="350")
		self.ModifFrame.pack(side=TOP)
		self.ModifFrame.pack_propagate(False)

		self.buttonConnectFrame= Frame(self.bottomFrame,height="50",width="550")
		self.buttonConnectFrame.pack(side=TOP)

		#-- IMAGE --#
		
		self.imageLabelMP = Label(self.ModifFrame, image = self.userImage ,width=100,height=80)
		self.imageLabelMP.grid(column=0,row=1,rowspan=1,columnspan= 2,padx = 10, pady=8, sticky=(N,W))

		l = Label(self.ModifFrame,text="Image actuelle",width=16,justify=CENTER,font=("Comic sans ms","12","bold"))
		l.grid(column=0,row=0 ,columnspan= 2,rowspan=1, sticky=(N,W))

		b = Button(self.ModifFrame,font=("Comic sans ms","12","bold"), text="Choisir une image",width=16, command=lambda:self.askopenfile())
		b.grid(column=0,row=2 ,columnspan= 2,rowspan=1,padx=5, pady=5, sticky=(N,W))

		#-- Biographie --#

		l = Label(self.ModifFrame,text="Biographie: ",width=15,anchor=W,font=("Comic sans ms","12","bold")) 
		l.grid(column=3,row=0 ,columnspan= 2, sticky=(N,W))
		t = Text(self.ModifFrame,height=7,width=26 )
		t.grid(column=2,row=1 ,columnspan= 3,rowspan=1, sticky=(N,W))
		t.insert(INSERT,infoUser[8])

		#-- Sexe --#
		self.sexeHomme = Checkbutton(self.ModifFrame,text="Homme", command=lambda:self.changeSexe("Homme"))
		self.sexeHomme.grid(column=2,row=2 ,columnspan= 1,rowspan=1,padx=1,pady=10, sticky=(N,W))
		self.sexeHomme.deselect()
		self.sexeFemme = Checkbutton(self.ModifFrame,text="Femme", command=lambda:self.changeSexe("Femme"))
		self.sexeFemme.grid(column=3,row=2 ,columnspan= 1,rowspan=1,padx=1,pady=10, sticky=(N,W))
		self.sexeFemme.deselect()
		self.sexeAutre = Checkbutton(self.ModifFrame,text="Autre", command=lambda:self.changeSexe("Autre"))
		self.sexeAutre.grid(column=4,row=2 ,columnspan= 1,rowspan=1,padx=1,pady=10, sticky=(N,W))
		self.sexeAutre.deselect()
		self.sexe = infoUser[7]
		if infoUser[7] == "Homme":
			self.sexeHomme.select()
		elif infoUser[7] == "Femme":
			self.sexeFemme.select()
		elif infoUser[7] == "Autre":
			self.sexeAutre.select()


		#-- Prénom --#
		self.firstNameIns.set(infoUser[3])
		l = Label(self.ModifFrame,text="Prénom: ",width=25,anchor=W,font=("Comic sans ms","12","bold"))
		l.grid(column=0,row=4 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.ModifFrame,font=("Arial","12"), width=15, textvariable=self.firstNameIns)
		e.grid(column=3,row=4 ,columnspan= 2, pady=8, sticky=(N,W))

		
		#-- Nom --#
		self.LastNameIns.set(infoUser[4])
		l = Label(self.ModifFrame,text="Nom: ",width=25,anchor=W,font=("Comic sans ms","12","bold"))
		l.grid(column=0,row=5 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.ModifFrame,font=("Arial","12"), width=15, textvariable=self.LastNameIns)
		e.grid(column=3,row=5 ,columnspan= 2, pady=8, sticky=(N,W))

		
		#-- Mot de passe --#

		l = Label(self.ModifFrame,text="Mot de passe: ",width=25,anchor=W,font=("Comic sans ms","12","bold"))
		l.grid(column=0,row=6 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.ModifFrame,font=("Arial","12"),show="*", width=15, textvariable=self.passwordIns)
		e.grid(column=3,row=6 ,columnspan= 2, pady=8, sticky=(N,W))

		
		#-- confirmation du mot de passe --#

		l = Label(self.ModifFrame,text="Confirmation mot de passe:",width=25,anchor=W,font=("Comic sans ms","12","bold"))
		l.grid(column=0,row=7 ,columnspan= 3, pady=8, sticky=(N,W))
		e = Entry(self.ModifFrame,font=("Arial","12"),show="*", width=15, textvariable=self.passwordInsVerif)
		e.grid(column=3,row=7 ,columnspan= 2, pady=8, sticky=(N,W))

		
	
		#-- boutton --#

		b = Button(self.ModifFrame,font=("Comic sans ms","12","bold"), text="Retour",width=8, height=1,command=lambda:self.changePage("Profil",self.controleur.getUsername()))
		b.grid(column=0,row=8 ,columnspan= 1,rowspan=1, pady=20, sticky=(N,W))


		
		b = Button(self.ModifFrame,font=("Comic sans ms","12","bold"), text="Modifier",width=8, height=1, command=lambda:self.modify(t.get(START,END)))
		b.grid(column=1,row=8 ,columnspan= 2,rowspan=2, pady=20, sticky=(N,W))
		
		#Bouton pour se désabonner
		d = Button(self.ModifFrame,font=("Comic sans ms","12","bold"), text="Désabonner",width=8, height=1,command=lambda:self.deleteAccount(self.controleur.getId()))
		d.grid(column=3,row=8,columnspan=2,rowspan=1, pady=20,sticky=(N,W))


		
	def showProfile(self,event,user):
		self.changePage("Profil",user)
		
	def showFriendPage(self):		
		self.headerContent()
		self.newFriend()
		# Frame contenant la scrollbar
		self.scrollFrame =  Frame(self.bottomFrame,width="20")
		self.scrollFrame.pack(fill=Y,side=RIGHT)
		# Scrollbar
		self.scrollbar = ttk.Scrollbar(self.scrollFrame)
		self.scrollbar.pack(expand=1,fill=Y)
		
		canvas = Canvas(self.bottomFrame, bd=0, highlightthickness=0, yscrollcommand=self.scrollbar.set)
		canvas.pack(expand=1, fill=BOTH)
		self.scrollbar.config(command=canvas.yview)
		
		canvas.xview_moveto(0)
		canvas.yview_moveto(0)
		# Frame de gauche avec les messages
		self.messagesFrame = Frame(canvas, padx="10",pady="10")
		messagesFrameId = canvas.create_window(0, 0, window=self.messagesFrame, anchor=NW)
		
		# Pour permettre la scrollbar sur le frame : http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
		def _configure_interior(event):
			# Ajuste la barre de défilement pour qu'elle prenne la bonne taille
			size = (self.messagesFrame.winfo_reqwidth(), self.messagesFrame.winfo_reqheight())
			canvas.config(scrollregion="0 0 %s %s" % size)
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Canvas avec celle du Frame
				canvas.config(width=self.messagesFrame.winfo_reqwidth())
		self.messagesFrame.bind('<Configure>', _configure_interior)
		def _configure_canvas(event):
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Frame pour remplir le Canvas
				canvas.itemconfigure(messagesFrameId, width=canvas.winfo_width())
		canvas.bind('<Configure>', _configure_canvas)
		
	def showHomePage(self):
		
		self.headerContent()
		f = Frame(self.bottomFrame,width="150")
		f.pack(side=TOP,fill=X)
		self.imageRefresh = PhotoImage(file="refresh.gif",width="36",height="36")
		imageLabel = Label(f, cursor="hand2", image = self.imageRefresh)
		imageLabel.bind("<1>", lambda event, text=imageLabel: self.changePage("Accueil"))
		imageLabel.pack(side=LEFT)
		# Frame contenant la scrollbar
		self.scrollFrame =  Frame(self.bottomFrame,width="20")
		self.scrollFrame.pack(fill=Y,side=RIGHT)
		# Scrollbar
		self.scrollbar = ttk.Scrollbar(self.scrollFrame)
		self.scrollbar.pack(expand=1,fill=Y)
		canvas = Canvas(self.bottomFrame, bd=0, highlightthickness=0, yscrollcommand=self.scrollbar.set)
		canvas.pack(expand=1, fill=BOTH)
		self.scrollbar.config(command=canvas.yview)
		
		canvas.xview_moveto(0)
		canvas.yview_moveto(0)
		# Frame de gauche avec les messages
		self.messagesFrame = Frame(canvas, padx="10",pady="10")
		messagesFrameId = canvas.create_window(0, 0, window=self.messagesFrame, anchor=NW)
		
		# Pour permettre la scrollbar sur le frame : http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
		def _configure_interior(event):
			# Ajuste la barre de défilement pour qu'elle prenne la bonne taille
			size = (self.messagesFrame.winfo_reqwidth(), self.messagesFrame.winfo_reqheight())
			canvas.config(scrollregion="0 0 %s %s" % size)
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Canvas avec celle du Frame
				canvas.config(width=self.messagesFrame.winfo_reqwidth())
		self.messagesFrame.bind('<Configure>', _configure_interior)
		def _configure_canvas(event):
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Frame pour remplir le Canvas
				canvas.itemconfigure(messagesFrameId, width=canvas.winfo_width())
		canvas.bind('<Configure>', _configure_canvas)
		self.dictMessage = dict()
		self.dictImage = dict()
		
	def showProfilePage(self, username,userInfo, email):
		self.headerContent()
		
		self.profileFrame = Frame(self.bottomFrame)
		self.profileFrame.pack(fill=X)
		self.userProfile(username,userInfo, email)
		if username == self.controleur.getUsername():
			self.newMessage()
		self.bottomFrameProfile = Frame(self.bottomFrame, padx=10, pady=10)
		self.bottomFrameProfile.pack(fill=BOTH,side=BOTTOM, expand=1)
		
		# Frame contenant la scrollbar
		self.scrollFrame =  Frame(self.bottomFrameProfile,width="20")
		self.scrollFrame.pack(fill=Y,side=RIGHT)
		# Scrollbar
		self.scrollbar = ttk.Scrollbar(self.scrollFrame)
		self.scrollbar.pack(expand=1,fill=Y)
		
		canvas = Canvas(self.bottomFrameProfile, bd=0, highlightthickness=0, yscrollcommand=self.scrollbar.set)
		canvas.pack(fill=BOTH, expand=1)
		
		self.scrollbar.config(command=canvas.yview)
		
		canvas.xview_moveto(0)
		canvas.yview_moveto(0)
		# Frame de gauche avec les messages
		self.messagesFrame = Frame(canvas, padx="10",pady="10")
		messagesFrameId = canvas.create_window(0, 0, window=self.messagesFrame, anchor=NW)
		
		# Pour permettre la scrollbar sur le frame : http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
		def _configure_interior(event):
			# Ajuste la barre de défilement pour qu'elle prenne la bonne taille
			size = (self.messagesFrame.winfo_reqwidth(), self.messagesFrame.winfo_reqheight())
			canvas.config(scrollregion="0 0 %s %s" % size)
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Canvas avec celle du Frame
				canvas.config(width=self.messagesFrame.winfo_reqwidth())
		self.messagesFrame.bind('<Configure>', _configure_interior)
		def _configure_canvas(event):
			if self.messagesFrame.winfo_reqwidth() != canvas.winfo_width():
				# Ajuste la grosseur du Frame pour remplir le Canvas
				canvas.itemconfigure(messagesFrameId, width=canvas.winfo_width())
		canvas.bind('<Configure>', _configure_canvas)
		self.dictMessage = dict()
		self.dictImage = dict()

	def register(self):
		self.controleur.register(self.userIns.get(),self.passwordIns.get(),self.firstNameIns.get(),self.LastNameIns.get(),self.emailIns.get())
	

	def modify(self,texte): # modification du profil
		self.controleur.modifyAccount(self.firstNameIns.get(),self.LastNameIns.get(),self.passwordIns.get(),self.passwordInsVerif.get(),self.sexe,texte)

	def	newMessage(self):
		newMessage = Frame(self.profileFrame, padx=10,pady=10)
		newMessage.pack(expand=1, fill=X)
		# Frame pour un message avec son nom d'utilisateur et son avatar
		newMessageFrame = Frame(newMessage, padx="10", pady="10", background="white",borderwidth=2, relief=GROOVE)
		newMessageFrame.pack(fill=X,side=TOP,expand=1) 
		# Nom d'utilisateur si on clique sa nous envoie vers son profil
		userLabel = Label(newMessageFrame,text="Message:", anchor=W, background="white", font = ("Comic Sans ms", "14","underline bold"))
		userLabel.pack(side=TOP)
		# Zone de texte contenant le message
		email = Text(newMessageFrame, height=4,background ="#FFF")
		email.pack(fill=BOTH,side=TOP)
		Button(newMessageFrame, text="Envoyer",command=lambda:self.sendMessage(email.get(START,END))).pack(side=TOP)
	
	def	newFriend(self):
		newFriend = Frame(self.bottomFrame, padx=10,pady=10)
		newFriend.pack(fill=X)
		# Frame pour un message avec son nom d'utilisateur et son avatar
		newFriendFrame = Frame(newFriend, padx="10", pady="10", background="white",borderwidth=2, relief=GROOVE)
		newFriendFrame.pack(fill=X,side=TOP,expand=1) 
		# Nom d'utilisateur si on clique sa nous envoie vers son profil
		label = Label(newFriendFrame,text="Ajouter un ami:", anchor=W, background="white", font = ("Comic Sans ms", "14","underline bold"))
		label.pack(side=LEFT)
		Frame(newFriendFrame, width=10, padx=10,pady=10).pack(side=LEFT)
		# Zone de texte contenant le message
		text = Text(newFriendFrame, height=1,background ="#FFF", width=28)
		text.pack(side=LEFT)
		Frame(newFriendFrame, width=10, padx=10,pady=10).pack(side=LEFT)
		Button(newFriendFrame, text="Ajouter",command=lambda:self.addFriend(text.get(START,END))).pack(side=LEFT)
		
	def addFriend(self,friend):
		self.controleur.addFriend(friend)
		self.changePage("Amis")
		
	def sendMessage(self,message): # appel controleur pour l'envoi des messages a la base de donnée
		self.controleur.sendMessage(message)
		self.changePage("Profil",self.controleur.getUsername())
		
	def userProfile(self, user,infoUser, courriel):
		self.userImageTemp = None
		
		self.imageEdit = PhotoImage(file="edit.ppm",width="32",height="32")
		self.imageSave = PhotoImage(file="save.ppm",width="32",height="32")
		# Frame pour un message avec son nom d'utilisateur et son avatar
		messageFullFrame = Frame(self.profileFrame, padx="10", pady="10", background="white",borderwidth=2, relief=GROOVE)
		messageFullFrame.pack(fill=X,side=TOP,expand=1)
		# Avatar
		if infoUser[10] != "":
			self.userImageTemp = PhotoImage(data=infoUser[10],width="80",height="80")
		else:
			self.userImageTemp = self.defaultImage
		self.imageLabelMP = Label(messageFullFrame, image = self.userImageTemp, background="white")
		self.imageLabelMP.pack(fill=BOTH,side=LEFT)
		# Frame pour le nom d'utilisateur et le message
		messageFrame = Frame(messageFullFrame, height="40", padx="10", background="white")
		messageFrame.pack(fill=X,side=TOP)
		# Frame vide pour éviter qu'on puisse cliquer à droite du nom d'utilisateur
		emptyFrame = Frame(messageFrame, height = 14, background="white")
		emptyFrame.pack(fill=X, side=TOP, expand=1)
		# Nom d'utilisateur si on clique sa nous envoie vers son profil
		userLabel = Label(emptyFrame,text=user, anchor=W, background="white", font = ("Comic Sans ms", "14","underline bold"))
		userLabel.pack(side=LEFT)
		userLabel.pack_propagate(False)
		# Zone de texte contenant le message
		email = Label(messageFrame,text = infoUser[9], height=1, borderwidth=0,anchor=W, background="white")
		email.pack(fill = X)
		name = Label(messageFrame,text = infoUser[3]+' '+infoUser[4] + '  ('+ str(infoUser[7])+')', height=1, borderwidth=0,anchor=W, background="white")
		name.pack( fill = X)
		bio = Label(messageFrame,text = 'Biographie : '+infoUser[8], height=1, borderwidth=0,anchor=W, background="white")
		bio.pack( fill = X)
		age = self.controleur.calculateAge(infoUser[6])
		ageLabel = Label(messageFrame,text= 'Âge : '+age,height=1, borderwidth=0,anchor=W,background="white")
		ageLabel.pack(fill=X)
		if user == self.controleur.getUsername():
			editLabel = Label(messageFullFrame, cursor="hand2", image = self.imageEdit, background="white",)
			editLabel.bind("<1>", lambda event :  self.changePage("Modification"))
			editLabel.pack(fill=BOTH,side=RIGHT)
		
	def message(self,id, user, content, DateMessage, image = None):
		if image is None:
			self.imageList.append(self.defaultImage)
		else:
			self.imageList.append(PhotoImage(data=image,width="80",height="80"))
		# Frame pour un message avec son nom d'utilisateur et son avatar
		messageFullFrame = Frame(self.messagesFrame, padx="10", pady="7", background="white",borderwidth=2, relief=GROOVE)
		messageFullFrame.pack(fill=X,side=TOP,expand=1)
		# Avatar
		imageLabel = Label(messageFullFrame, cursor="hand2", image = self.imageList[-1], background="white")
		imageLabel.bind("<1>", lambda event :  self.showProfile(event,user))
		imageLabel.pack(fill=BOTH,side=LEFT)
		# Frame pour le nom d'utilisateur et le message
		messageFrame = Frame(messageFullFrame, height="40", padx="10", background="white")
		messageFrame.pack(fill=X,side=TOP)
		# Frame vide pour éviter qu'on puisse cliquer à droite du nom d'utilisateur
		emptyFrame = Frame(messageFrame, height = 14, background="white")
		emptyFrame.pack(fill=X, side=TOP, expand=1)
		# Nom d'utilisateur si on clique sa nous envoie vers son profil
		userLabel = Label(emptyFrame,text=user, cursor="hand2", anchor=W, background="white", font = ("Comic Sans ms", "14","underline bold"))
		userLabel.bind("<1>", lambda event :  self.showProfile(event,user))
		userLabel.pack(side=LEFT)
		userLabel.pack_propagate(False)
		# Ajout du X de la suppression
		if user == self.controleur.getUsername():
			# Bouton X
			xLabel = Label(emptyFrame,text="X", cursor="hand2",anchor=W, background="white", font = ("Comic Sans ms", "12","bold"))
			xLabel.bind("<1>", lambda event :  self.deleteMessage(id))
			xLabel.pack(side=RIGHT)
			xLabel.pack_propagate(False)
			# Bouton Edit
			self.dictImage[id] = editLabel = Label(emptyFrame, anchor=W, background="white", cursor="hand2", image = self.imageEdit)
			editLabel.bind("<1>", lambda event : self.editMessage(id))
			editLabel.pack(side=RIGHT)
			editLabel.pack_propagate(False)
		# Ajout de la date sur le message
		dateLabel = Label(emptyFrame,text=DateMessage,padx="20",anchor=W, background="white", font = ("Arial", "8","italic"))
		dateLabel.pack(side=RIGHT)
		dateLabel.pack_propagate(False)
		# Zone de texte contenant le message
		self.dictMessage[id] = message = Text(messageFrame, height=3, borderwidth=0, background="white")
		message.insert(START,content)
		message.config(state="disabled")
		message.pack(fill=BOTH,side=TOP)
		# Frame de séparation
		Frame(self.messagesFrame, height=5, padx="10").pack()
		
	def friend(self, user, image = None):
		if image is None:
			self.imageList.append(self.defaultImage)
		else:
			self.imageList.append(PhotoImage(data=image,width="80",height="80"))
		# Frame pour un message avec son nom d'utilisateur et son avatar
		messageFullFrame = Frame(self.messagesFrame, padx="10", pady="10", background="white",borderwidth=2, relief=GROOVE)
		messageFullFrame.pack(fill=X,side=TOP,expand=1)
		# Avatar
		imageLabel = Label(messageFullFrame, cursor="hand2", image = self.imageList[-1], background="white")
		imageLabel.bind("<1>", lambda event : self.showProfile(event,user))
		imageLabel.pack(fill=BOTH,side=LEFT)
		# Frame pour le nom d'utilisateur et le message
		messageFrame = Frame(messageFullFrame, height="40", padx="10", background="white")
		messageFrame.pack(fill=X,side=TOP)
		# Frame vide pour éviter qu'on puisse cliquer à droite du nom d'utilisateur
		emptyFrame = Frame(messageFrame, height = 14, background="white")
		emptyFrame.pack(fill=X, side=TOP, expand=1)
		#logo pour supprimer un ami
		dateLabel = Label(emptyFrame,text="X", cursor="hand2",anchor=W, background="white", font = ("Comic Sans ms", "12","bold"))
		dateLabel.bind("<1>", lambda event : self.deleteFriend(user))
		dateLabel.pack(side=RIGHT)		
		
		# Nom d'utilisateur si on clique sa nous envoie vers son profil
		userLabel = Label(emptyFrame,text=user, cursor="hand2",anchor=W, background="white", font = ("Comic Sans ms", "14","underline bold"))
		userLabel.bind("<1>", lambda event : self.showProfile(event,user))
		userLabel.pack(side=LEFT)
		userLabel.pack_propagate(False)
		# Frame de séparation
		Frame(self.messagesFrame, height=5, padx="10").pack()
		
	def changePage(self,page, username = "none"):
		if page is not "current":
			self.pageLabel.config(text = page)
			self.pageLabel.update_idletasks()
		if self.barre is not None:
			self.barre.destroy()
		if self.menuButton is not None:
			self.menuButton.destroy()
		self.reinitializeValue()
		self.bottomFrame.destroy()
		self.createSecondFrames()
		self.controleur.changePage(page, username)


	def changeSexe(self,sexe):
		if sexe is "Homme":
			self.sexe = "Homme"
			self.sexeFemme.deselect()
			self.sexeAutre.deselect()
			self.sexeHomme.select()
		elif sexe is "Femme":
			self.sexe = "Femme"
			self.sexeHomme.deselect()
			self.sexeAutre.deselect()
			self.sexeFemme.select()
		else:
			self.sexe = "Autre"
			self.sexeHomme.deselect()
			self.sexeAutre.select()
			self.sexeFemme.deselect()


	def reinitializeValue(self):
		self.user.set("")
		self.password.set("")
		self.userIns.set("")
		self.passwordIns.set("")
		self.passwordInsVerif.set("")
		self.firstNameIns.set("")
		self.LastNameIns.set("")
		self.emailIns.set("")
		self.erreur.set("")
			

	def showError(self,errorMessage,couleur ="#9D0000"):
		messagebox.showerror("Erreur", errorMessage)
	
	def showInfo(self, info):
		messagebox.showinfo("Information", info)

	def askopenfile(self):
		im = self.controleur.changeImage(askopenfile().name)
		self.updateImageUser(im)
		
	def updateImageUser(self, image):
		self.userImage = PhotoImage(data=image,width="80",height="80")
		if self.menuButton is not None:
			self.menuButton.config(image = self.userImage)
			self.menuButton.update_idletasks()
		if self.imageLabelMP is not None:
			self.imageLabelMP.config(image = self.userImage)
			self.imageLabelMP.update_idletasks()
	
	def deleteAccount(self,id):
		result = messagebox.askyesno("Suppression du compte: "+ str(id), "Êtes-vous certains de vouloir supprimer votre compte?")
		if result:
			self.controleur.removeAccount(id)
			self.changePage(("Connexion"))
			messagebox.showinfo("Compte", "Votre compte à été correctement supprimé")
			
	def deleteFriend(self,nom):
		result = messagebox.askyesno("Suppression d'un ami: "+ nom, "Êtes-vous certains de vouloir supprimer cet ami?")
		if result:
			self.controleur.deleteFriend(nom)
			
	def deleteMessage(self,id):
		result = messagebox.askyesno("Suppression d'un message", "Êtes-vous certains de vouloir supprimer ce message?")
		if result:
			self.controleur.deleteMessage(id)
			
	def editMessage(self,id):
		self.dictMessage[id].config(state="normal", background="#FFF", borderwidth=1)
		self.dictImage[id].config(image = self.imageSave)
		self.dictImage[id].bind("<1>", lambda event : self.updateMessage(self.dictMessage[id].get(START,END),id))
	def updateMessage(self,message,id):
		self.controleur.editMessage(message,id)
		self.changePage("current",self.controleur.getUsername())
	def launch(self):
		self.root.mainloop()



'''
def start():
	vue = Vue()
	vue.launch()
'''