'''
Auteur: David Lebrun, Jeff Massey, Antoine Martin, Francis St-Onge
Titre: Phase 2 - Projet Oracle
Remise: 26 avril 2013
Fichier: Controleur.py
'''
# -*- coding: utf-8 -*-
from Modele import *
from Vue import *

class Controleur:

	def __init__(self):
		self.modele = Modele(self)
		self.vue = Vue(self)
		self.currentPage = "Connexion"
		self.firstConnection = 0 # variable qui retient si l'utilisateur c'est connecté au moins une fois
		#self.modele.inputInfo()
		self.changePage(self.currentPage, "none")
		#démarre le frame de l'application
		self.vue.launch()
		#Connect appelé de la vue et appelle le modèle. 

	def connect(self, username, password):
		#La méthode authenticate s'occupe d'appeler la méthode connexion pour se connecter
		self.modele.connection() # connection a la db oracle
		if self.modele.authenticate(username, password):
			self.vue.changePage("Profil", username)
			im = self.modele.getImageUser()
			self.vue.updateImageUser(im)
		else:
			print("Erreur connexion")
		self.modele.disconnection()#deconnection a la db oracle
		self.firstConnection = 1
	def changePage(self,page, username = "none"):
		if page is not "current":
			self.currentPage = page
		if self.currentPage is "Accueil":
			self.vue.showHomePage()
			messages = self.modele.recevoirAllMessage()
			self.vue.imageList= []
			if messages:
				for message in messages:
					self.vue.message(message[0],message[4],message[1],message[2],message[5])
		elif self.currentPage is "Connexion":
			self.vue.showConnectionPage()
		elif self.currentPage is "Inscription":
			self.vue.showInscriptionPage()
		elif self.currentPage is "Profil":
			if self.firstConnection == 1:
				self.modele.connection() # connection a la db oracle
			messages = self.modele.recevoirMessage(username)
			self.vue.imageList= []
			if username == "none":
				username = self.modele.username
			email = self.modele.email
			self.vue.showProfilePage(username,self.modele.getUser("",username),email)
			if messages:
				for message in messages:
					self.vue.message(message[0],message[4],message[1],message[2],message[5])
			if self.firstConnection == 1:
				self.modele.disconnection()#deconnection a la db oracle
		elif self.currentPage is "Modification":
			self.modele.connection() # connection a la db oracle
			self.vue.showModifPage(self.modele.getUser("",self.modele.username))
			self.modele.disconnection()#deconnection a la db oracle
		elif self.currentPage is "Amis":
			amis = self.modele.listeFriends(self.modele.id)
			self.vue.imageList= []
			self.vue.showFriendPage()
			if amis:
				for ami in amis:
					self.vue.friend(ami[0],ami[1])
		else:
			self.vue.showHomePage()

	def register(self, username, password, firstname, lastname, email,sexe):
		register = self.modele.register(username,password,firstname,lastname,email,sexe)

		if register == True:
			self.vue.changePage("Connexion", "none")
	
	def removeAccount(self,id):
		self.modele.removeAccount(id)

	def showError(self, e):
		self.vue.showError(e)
	
	def showInfo(self, info):
		self.vue.showInfo(info)

	def receiveAllMessage(self):
		self.modele.recevoirAllMessage()

	def changeImage(self,url):
		return self.modele.changeImage(url)
		
	def addFriend(self, friend):
		self.modele.addFriend(friend.rstrip('\n'))
		
	def modifyAccount(self,prenom,nom,mdp,mdp2):
		if mdp == mdp2:
			if self.modele.modifyProfil(prenom,nom,mdp):
				self.vue.showError('Modification effectué',"#00CD00")
		else:
			self.vue.showError('Les mots de passe doivent être identiques',"#ff0000")

			
	def deleteFriend(self,nom):
		self.modele.effacerAmi(nom)
		self.vue.changePage("Amis")
		
	def deleteMessage(self,id):
		self.modele.effaceMessage(id)
		self.vue.changePage("current",self.getUsername())
	
	def sendMessage(self,message):
		self.modele.envoyerMessage(message.rstrip('\n'))
		
	def editMessage(self,message,id):
		self.modele.modifierMessage(message.rstrip('\n'), id)
		
	def getId(self):
		return self.modele.getId()
		
	def getUsername(self):
		return self.modele.getUsername()
		
	def getUserInfo(self,userName):
		return self.modele.getUser("",userName)
		
	def calculateAge(self, dateNaissance):
		return self.modele.calculateAge(dateNaissance)

if __name__ == '__main__':
	c = Controleur()
