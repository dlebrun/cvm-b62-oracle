'''
Auteur: David Lebrun, Jeff Massey, Antoine Martin, Francis St-Onge
Titre: Phase 2 - Projet Oracle
Remise: 26 avril 2013
Fichier: Modele.py
'''
# -*- coding: utf-8 -*-
P = 'C:/Oracle/client/product/11.2.0/client_1/BIN'
import cx_Oracle
import constant
import time
import datetime
import base64
import os
from datetime import date

class Modele:

	def __init__(self, parent):
		self.parent = parent
		self.id = ""
		self.username = ""
		self.email = ""
		self.time = [] #variable de temps!

	#on se connecte à la database
	def connection(self):
		#Les variables constantes 
		username = constant.Username
		password = constant.Password
		host = constant.Host
		port = constant.Port
		sid = constant.SID
		try:
			self.dsn_tns = cx_Oracle.makedsn(host,port,sid)
			self.chainConnect = username + '/' + password + '@' + self.dsn_tns
			self.connect = cx_Oracle.connect(self.chainConnect)
			self.curseur = self.connect.cursor()
			print('Connexion réussi')
		except Exception as e:
			print("Erreur avec la méthode connection: "+e) #faire afficher un message d'erreur
		
			
	#Permet de déconnecter la db
	def disconnection(self):
		print ("Déconne")
		self.connect.close()
		print('Déconnexion réussi')

	def authenticate(self, username, password):
		#on perpare le query
		query = ("SELECT * FROM USERS WHERE USERNAME ={0} AND PASSWORD ={1}")
		#on rajoute les variables a la query
		finalQuery = query.format(self.oracleFormat(username), self.oracleFormat(password))
		#On execute la query
		try:
			self.curseur.execute(finalQuery)
			liste = [] #liste qui contiendra les valeurs de la db
			for line in self.curseur.fetchall():
				liste.append(line)
			if liste:
				self.id = liste[0][0]
				self.username = liste[0][1]
				self.email = liste[0][9]
				#self.disconnection()
				return True
			else:
				self.parent.showError("Nom d'usager / Mot de passe invalide")
				#self.disconnection()
				return False
		except Exception as e:
			self.parent.showError(e)
			
	def register(self, username, password, firstname, lastname, email,sexe):
		self.connection()

		queryRegister = ("INSERT INTO USERS VALUES (seq_id_users.nextval, {0} , {1} , {2} , {3},(to_date({4}, 'yyyy/mm/dd hh24:mi:ss')),'', {5},' ',{6},'')")
		queryFriend = ("INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, seq_id_users.currval,seq_id_users.currval)")

		finalQueryRegister = queryRegister.format(self.oracleFormat(username),self.oracleFormat(password), self.oracleFormat(firstname), self.oracleFormat(lastname),self.getDateOracleFormat(),self.oracleFormat(sexe),self.oracleFormat(email))

		try:
			self.curseur.execute(finalQueryRegister)
			self.connect.commit()
			self.curseur.execute(queryFriend)
			self.connect.commit()

			f = open("user.gif", 'rb')
			s = f.read()
			f.close();
			try:
				self.connection()
				bigFileVar = self.curseur.var(cx_Oracle.BLOB)
				bigFileVar.setvalue(0,s)
				self.curseur.setinputsizes(file_data = cx_Oracle.BLOB)
				self.curseur.execute("UPDATE USERS SET IMAGE = :file_data WHERE USERS.USERNAME = :attachment_USERNAME",attachment_USERNAME = username, file_data = bigFileVar)
				self.connect.commit()			
				
			except Exception as e:
				self.parent.showError(e)
				
			self.disconnection()
			return True
		except Exception as e:
			self.parent.showError("Un ou des champs sont manquant")
			return False
		self.disconnection()
		
	
	def modifyProfil(self,prenom,nom,mdp):
		if prenom:
			query = ("UPDATE USERS SET FIRSTNAME={0}  WHERE ID={1}")
			finalQuery = query.format(self.oracleFormat(prenom),self.id)
			if nom: 
				query = ("UPDATE USERS SET FIRSTNAME={0},LASTNAME={1}  WHERE ID={2}")
				finalQuery = query.format(self.oracleFormat(prenom),self.oracleFormat(nom),self.id)
				if mdp:
					query = ("UPDATE USERS SET FIRSTNAME={0},LASTNAME={1}, PASSWORD={2}  WHERE ID={3}")
					finalQuery = query.format(self.oracleFormat(prenom),self.oracleFormat(nom),self.oracleFormat(mdp),self.id)
			elif mdp:
				query = ("UPDATE USERS SET FIRSTNAME={0}, PASSWORD={1}  WHERE ID={2}")
				finalQuery = query.format(self.oracleFormat(prenom),self.oracleFormat(mdp),self.id)
		elif nom:
			query = ("UPDATE USERS SET LASTNAME={0}  WHERE ID={1}")
			finalQuery = query.format(self.oracleFormat(nom),self.id)	
			if mdp:
				query = ("UPDATE USERS SET LASTNAME={0},PASSWORD={1}  WHERE ID={2}")
				finalQuery = query.format(self.oracleFormat(nom),self.oracleFormat(mdp),self.id)
		elif mdp:
			query = ("UPDATE USERS SET PASSWORD={0}  WHERE ID={1}")
			finalQuery = query.format(self.oracleFormat(mdp),self.id)
			
		else:
			print("non mod")
	
		self.connection()
		try:
			self.curseur.execute(finalQuery)
			self.connect.commit()
			self.disconnection()
			return True
		except Exception as e:
			self.parent.showError("Erreur lors de la modification")
			self.disconnection()
			return False
		self.disconnection()

	def removeAccount(self,id):
		self.connection()
		
		query = ("DELETE FROM USERS WHERE ID = {0}") 
		finalQuery = query.format(self.oracleFormat(id))

		try:
			self.curseur.execute(finalQuery)
			self.connect.commit()
			self.disconnection()
			return True
		except Exception as e:
			self.parent.showError(e)
			print(e)
			self.disconnection()
			return False

		self.disconnection()

	def addFriend(self, friend): # ajoute un ami à la base de donné avec le username
		
		listeAmi = self.listeFriends(self.id)	#Retourne la liste d'amis de l'utilisateur friend
		i=0
		for ami in listeAmi:
			if listeAmi[i][0] == friend:		#Si l'utilisateur i de la liste est équivalent a friend alors 
				ajoute = 0						#ajoute retourne 0 et on affiche un message qui informe que l'amis est deja dans la liste
				break
			elif friend == self.username:
				ajoute = 2
				break
			else:
				ajoute = 1
			i+=1
		
		if ajoute == 1:							#Si l'amis n'est pas dans la liste on peut l'ajouter
			self.connection()
			query = ("SELECT ID FROM USERS WHERE USERNAME = {0}")
			finalQuery = query.format(self.oracleFormat(friend))
			
			try:
				self.curseur.execute(finalQuery)
				liste = [] #liste qui contiendra les valeurs de la db
				for line in self.curseur.fetchall():
					liste.append(line)
				query = ("INSERT INTO FRIENDS VALUES (seq_id_friends.nextval,{0},{1})")
				finalQuery = query.format(self.oracleFormat(self.id),self.oracleFormat(liste[0][0]))
				self.curseur.execute(finalQuery)
				self.connect.commit()
				self.disconnection()
				self.parent.showInfo("L'utilisateur "+friend+" est désormais votre ami!")
				return True
			except Exception as e:
				self.parent.showError("Utilisateur inexistant")
				self.disconnection()
				return False
		elif ajoute == 2:
			self.parent.showError("Vous ne pouvez vous ajouter vous mêmes")
		else:
			self.parent.showError(friend+" est déjà dans votre liste d'amis")
			
			
			
	def effacerAmi(self,nomAmi): # efface un «mi de la base de donnée
		self.connection()
		query = ("SELECT ID FROM USERS WHERE USERNAME = {0}")
		finalQuery = query.format(self.oracleFormat(nomAmi))
		try:
			self.curseur.execute(finalQuery)
			self.connect.commit()
			liste = [] #liste qui contiendra les valeurs de la db
			for line in self.curseur.fetchall():
				liste.append(line)
			
			query = ("DELETE FROM FRIENDS WHERE ID_USERS = {0} AND ID_FRIENDS = {1}")
			finalQuery = query.format(self.oracleFormat(self.id),self.oracleFormat(liste[0][0]))
			self.curseur.execute(finalQuery)
			self.connect.commit()
			
			self.disconnection()
			
			return True
		except Exception as e:
			self.parent.showError(e)
			print(e)
			self.disconnection()
			return False
	
	#Efface un message de la base de donnée
	def effaceMessage(self, id):
		self.connection()
		query = ("DELETE FROM MESSAGE WHERE ID = {0}")
		finalQuery = query.format(self.oracleFormat(id))
		try:
			self.curseur.execute(finalQuery)
			self.connect.commit()
			self.disconnection()
			return True
		except Exception as e:
			self.parent.showError(e)
			self.disconnection()
			return False

	def envoyerMessage(self,message): # Fonction qui pertmet d'envoyer une message
		self.connection()
		queryMessage = ("INSERT INTO MESSAGE VALUES(seq_id_message.nextval, {0},(to_date({1}, 'yyyy/mm/dd hh24:mi:ss')),{2})")
		finalQueryMessage = queryMessage.format(self.oracleFormat(message),self.getDateOracleFormat(),self.oracleFormat(self.id))
		print(self.id)
		try:
			self.curseur.execute(finalQueryMessage)
			self.connect.commit()
			self.disconnection()
			return True
		except cx_Oracle.DatabaseError as e:
			error, = e.args
			if error.code == 1400:
				self.parent.showError("Le message ne peut être vide")
			elif error.code == 1756:
				self.parent.showError("Le message contient des caractères invalide")	
			else:
				self.parent.showError("Une erreur inconnue est survenue")				
			self.disconnection()
			return False
		self.disconnection()
		
	def modifierMessage(self, message, id): # permet la modification d'un message 
		query = ("UPDATE MESSAGE SET CONTENT={0} WHERE ID={1}")
		finalQuery = query.format(self.oracleFormat(message),self.oracleFormat(id))
		
		self.connection()
		try:
			self.curseur.execute(finalQuery)
			self.connect.commit()
			self.disconnection()
			return True
		except Exception as e:
			self.parent.showError("Erreur lors de la modification")
			self.disconnection()
		return False
		self.disconnection()
		
	def recevoirMessage(self, username): # va chercher les messages d'un utilisateur
		query = ("SELECT * FROM (SELECT MESSAGE.ID,MESSAGE.CONTENT, MESSAGE.DATEMESSAGE, MESSAGE.ID_SENDER, USERS.USERNAME, USERS.IMAGE FROM MESSAGE, USERS WHERE USERS.USERNAME = {0} AND MESSAGE.ID_SENDER = USERS.ID ORDER BY DATEMESSAGE DESC) WHERE ROWNUM < 11")
		finalQuery = query.format(self.oracleFormat(username))
		try:
			self.curseur.execute(finalQuery)
			listeMessage = [] 
			for line in self.curseur.fetchall():
				ligne = []
				ligne.append(line[0])
				ligne.append(line[1])
				ligne.append(line[2])
				ligne.append(line[3])
				ligne.append(line[4])
				ligne.append(self.getImage(line[5].read()))
				listeMessage.append(ligne)
			if listeMessage:
				return listeMessage
			else:
				print("Aucun Message")
				return None
		except Exception as e:
			self.parent.showError(e)


	def recevoirAllMessage(self): # va chercher les 10 derniers messages de nos amis et nous
		self.connection()
		query = ("SELECT * FROM (SELECT MESSAGE.ID,MESSAGE.CONTENT, MESSAGE.DATEMESSAGE, MESSAGE.ID_SENDER, USERS.USERNAME, USERS.IMAGE FROM MESSAGE, USERS, FRIENDS WHERE USERS.ID = FRIENDS.ID_FRIENDS AND FRIENDS.ID_USERS = {0} AND FRIENDS.ID_FRIENDS = MESSAGE.ID_SENDER ORDER BY DATEMESSAGE DESC) WHERE ROWNUM <11")
		
		finalQuery = query.format(self.oracleFormat(self.id))
		try:
			self.curseur.execute(finalQuery)
			listeMessage = [] 
			for line in self.curseur.fetchall():
				ligne = []
				ligne.append(line[0])
				ligne.append(line[1])
				ligne.append(line[2])
				ligne.append(line[3])
				ligne.append(line[4])
				ligne.append(self.getImage(line[5].read()))
				listeMessage.append(ligne)
			if listeMessage:
				#print(listeMessage)
				self.disconnection()
				return listeMessage
			else:
				print("Aucun Message")
				self.disconnection()
				return None
		except Exception as e:
			self.parent.showError(e)
			
	def listeFriends(self, userId): # va chercher la liste d'ami
		self.connection()
		query = ("SELECT USERS.USERNAME, USERS.IMAGE FROM USERS, FRIENDS WHERE USERS.ID = FRIENDS.ID_FRIENDS AND FRIENDS.ID_USERS = {0} AND FRIENDS.ID_FRIENDS != {0} ")
		finalQuery = query.format(self.oracleFormat(userId))
		try:
			self.curseur.execute(finalQuery)
			listeFriends = [] 
			for line in self.curseur.fetchall():
				ligne = []
				ligne.append(line[0])
				ligne.append(self.getImage(line[1].read()))
				listeFriends.append(ligne)
			if listeFriends:
				self.disconnection()
				return listeFriends
			else:
				print("Aucun Amis")
				self.disconnection()
				return None
		except Exception as e:
			self.parent.showError(e)
			
	def getUserByName(self, username): # va chercher les informations d'un ami par son nom
		self.connection()
		query = ("SELECT ID FROM USERS WHERE USERNAME = {0}")
		finalQuery = query.format(self.oracleFormat(username))
		try:
			self.curseur.execute(finalQuery)
			listeInfoUser = [] 
			for line in self.curseur.fetchall():
				listeInfoUser.append(line)
			if listeInfoUser:
				self.disconnection()
				return listeInfoUser[0][0]
			else:
				print("Aucune informations")
				self.disconnection()
				return None
		except Exception as e:
			self.parent.showError(e)
			
	def getUser(self, userId = "", Username = ""): #va chercher les informations d'un ami par soit son ami
		if userId != "":
			query = ("SELECT * FROM USERS WHERE ID = {0}")
			finalQuery = query.format(self.oracleFormat(userId))
		elif Username != "":
			query = ("SELECT * FROM USERS WHERE USERNAME = {0}")
			finalQuery = query.format(self.oracleFormat(Username))
		try:
			self.curseur.execute(finalQuery)
			listeInfoUser = [] 
			for line in self.curseur.fetchall():
				listeInfoUser.append(line[0])
				listeInfoUser.append(line[1])
				listeInfoUser.append(line[2])
				listeInfoUser.append(line[3])
				listeInfoUser.append(line[4])
				listeInfoUser.append(line[5])
				listeInfoUser.append(line[6])
				listeInfoUser.append(line[7])
				listeInfoUser.append(line[8])
				listeInfoUser.append(line[9])
				listeInfoUser.append(self.getImage(line[10].read()))
			if listeInfoUser:
				return listeInfoUser
			else:
				print("Aucune informations")
				return None
		except Exception as e:
			self.parent.showError(e)
			
	def getDate(self): # méthode pour aller chercher l'heure et la date
		t = datetime.datetime.now()
		return t
		
	def getDateOracleFormat(self): # méthode qui retourne la date et l'heure en format d'oracle pour une requête
		
		if self.time:
			del self.time[:]
		self.time.append(self.getDate().year)
		self.time.append(self.getDate().month)
		self.time.append(self.getDate().day)
		self.time.append(self.getDate().hour)
		self.time.append(self.getDate().minute)
		self.time.append(self.getDate().second)
		
		return self.oracleFormat(self.timeFormat(self.time[0])+"/"+self.timeFormat(self.time[1])+"/"+self.timeFormat(self.time[2])+" "+self.timeFormat(self.time[3])+":"+self.timeFormat(self.time[4])+":"+self.timeFormat(self.time[5]))
		
	def timeFormat(self,number): # méthode qui rajoute un zéro si l'heure ou la date ne possède qu'un chiffre
		if len(str(number)) ==1:
			number = "0"+str(number)
		return str(number)
	
	def oracleFormat(self,line): # Met '' autour de la variable pour oralce capricieux!!
		line = str(line)
		return "'"+line+"'"

	def changeImage(self,url):
		stats = os.stat(url)
		if stats.st_size < 20000:
			f = open(url, 'rb')
			s = f.read()
			f.close();
			try:
				self.connection()
				attachment_id = self.id
				bigFileVar = self.curseur.var(cx_Oracle.BLOB)
				bigFileVar.setvalue(0,s)
				self.curseur.setinputsizes(file_data = cx_Oracle.BLOB)
				self.curseur.execute("UPDATE USERS SET IMAGE = :file_data WHERE USERS.ID = :attachment_id",attachment_id = attachment_id, file_data = bigFileVar)
				self.connect.commit()
				self.disconnection()

				return self.getImage(s)

				
			except Exception as e:
				self.parent.showError(e)
		else:
			self.parent.showError('Image doit être au maximum 80px / 80px')


	def getImageUser(self, idUser = None):
		self.curseur.var(cx_Oracle.BLOB)
		if idUser is None:
			self.curseur.execute("SELECT IMAGE FROM USERS WHERE ID = :id",id = self.id)
		else:
			self.curseur.execute("SELECT IMAGE FROM USERS WHERE ID = :id",id = idUser)
		row = self.curseur.fetchone()

		image = row[0].read()
		return self.getImage(image)

	def getImage(self, imgBLOB):
		return base64.b64encode(imgBLOB)
	
	def getId(self):
		return self.id
		
	def getUsername(self):
		return self.username
	
	def calculateAge(self, dateNaissance):
		if dateNaissance != None:
			birthdate = str(dateNaissance)
			year = birthdate[:4]
			month = birthdate[5:7]
			day = birthdate[8:10]
			
			year = int(float(year))
			month = int(float(month))
			day = int(float(day))
		
			try:
				Today = datetime.date.today()
				Age = Today.year - year
				
				
				if Today.month < month:
					Age -= 1
				elif Today.month == month:
					if Today.day < day:
						Age -= 1
						
				return str(Age)+" ans"
			except:
				return 'Wrong date format'
		else:
			return "Indéfinie"

