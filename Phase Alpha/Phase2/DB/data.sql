/*
*	Data.sql
	Insertion des donn�es dans les tables
*	Projet Oracle
*/



INSERT INTO USERS VALUES (seq_id_users.nextval, 'Tony', 'a', 'Antoine', 'Martin',(to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')), '', 'Homme', 'Jaime les chiens � sourcis', 'tonymartin_inc@hotmail.com', '' );
INSERT INTO USERS VALUES (seq_id_users.nextval, 'David', 'a', 'David', 'Lebrun',(to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')), '', 'Homme', 'Vie ta vie', 'dave.lebr1@gmail.com', '' );
INSERT INTO USERS VALUES (seq_id_users.nextval, 'Francis', 'a', 'Francis', 'St-onge',(to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')), '', 'Homme', 'Jaime pikachu', 'webmastrefr1@gmail.com', '' );
INSERT INTO USERS VALUES (seq_id_users.nextval, 'Jeff', 'a', 'Jeff', 'Massey', (to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')), '', 'Homme', 'Jadore linformatique', 'jeff.massey.jm@gmail.com', '' );
INSERT INTO USERS VALUES (seq_id_users.nextval, 'Guido', 'a', 'Guido', 'Conti', (to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')), '', 'Homme', 'La communication avant tout', 'gconti@cvm.qc.ca', '' );

/* Ajout d'ami pour Tony******************/
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Tony'), (SELECT ID FROM USERS WHERE USERNAME = 'David'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Tony'), (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Tony'), (SELECT ID FROM USERS WHERE USERNAME = 'Francis'));

/* Ajout d'ami pour David***************/
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'David'), (SELECT ID FROM USERS WHERE USERNAME = 'Francis'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'David'), (SELECT ID FROM USERS WHERE USERNAME = 'Tony'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'David'), (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'));

/*Ajout d'ami pour Francis*/
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Francis'), (SELECT ID FROM USERS WHERE USERNAME = 'David'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Francis'), (SELECT ID FROM USERS WHERE USERNAME = 'Tony'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Francis'), (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'));

/*Ajout d'ami pour Jeff*/
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'), (SELECT ID FROM USERS WHERE USERNAME = 'David'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'), (SELECT ID FROM USERS WHERE USERNAME = 'Tony'));
INSERT INTO FRIENDS VALUES(seq_id_friends.nextval, (SELECT ID FROM USERS WHERE USERNAME = 'Jeff'), (SELECT ID FROM USERS WHERE USERNAME = 'Francis'));


/*Ajout de message */
/*De  David*/
INSERT INTO MESSAGE VALUES(seq_id_message.nextval, 'Salut le monde, ici David',(to_date('2013/03/28 12:11:44', 'yyyy/mm/dd hh24:mi:ss')),(SELECT ID FROM USERS WHERE USERNAME = 'David'));
/*De  Jeff*/
INSERT INTO MESSAGE VALUES(seq_id_message.nextval, 'Salut le monde, ici Jeff',(to_date('2013/03/28 13:11:44', 'yyyy/mm/dd hh24:mi:ss')),(SELECT ID FROM USERS WHERE USERNAME = 'Jeff'));
/*De  Francis*/
INSERT INTO MESSAGE VALUES(seq_id_message.nextval, 'Salut le monde, ici Francis',(to_date('2013/03/28 14:11:44', 'yyyy/mm/dd hh24:mi:ss')),(SELECT ID FROM USERS WHERE USERNAME = 'Francis'));
/*De  Tony*/
INSERT INTO MESSAGE VALUES(seq_id_message.nextval, 'Salut le monde, ici Tony',(to_date('2013/03/28 15:11:44', 'yyyy/mm/dd hh24:mi:ss')),(SELECT ID FROM USERS WHERE USERNAME = 'Tony'));

commit;