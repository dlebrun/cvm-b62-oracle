'''
Auteur: David Lebrun
Titre: Phase 1 - Projet Oracle
Remise: 19 mars 2013
'''
# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import ttk
from tkinter import messagebox


class Vue:
    
    def __init__(self, parent):
        self.root = Tk()
        self.root.title("Projet Oracle - David Lebrun")
        self.root.minsize(300,300)
        self.root.geometry("950x600")
        self.parent = parent
        self.username = StringVar()
        self.password = StringVar()
        self.host = StringVar()
        self.port = StringVar()
        self.sid = StringVar()
        
        self.request = StringVar()
        
        self.menuConnection()
        self.frame.grid()

    #On affiche les components pour le effectuer la connexion
    def menuConnection(self):
        self.createFrame()
        #Label
        ttk.Label(self.frame, text="Nom d'utilisateur: " ).grid(column=0, row=1, sticky=W)
        ttk.Label(self.frame, text="Mot de passe: " ).grid(column=0, row=2, sticky=W)
        ttk.Label(self.frame, text="Hôte: " ).grid(column=0, row=3, sticky=W)
        ttk.Label(self.frame, text="Port: " ).grid(column=0, row=4, sticky=W)
        ttk.Label(self.frame, text="SID: " ).grid(column=0, row=5, sticky=W)
        #Entry
        self.entrerNom = ttk.Entry(self.frame, width=10, textvariable=self.username).grid(column=2,row=1,padx=8,pady=8)
        ttk.Entry(self.frame, width=10, textvariable=self.password , show="*").grid(column=2,row=2 ,padx=8,pady=8)
        ttk.Entry(self.frame, width=10, textvariable=self.host).grid(column=2,row=3 ,padx=8,pady=8)
        ttk.Entry(self.frame, width=10, textvariable=self.port).grid(column=2,row=4 ,padx=8,pady=8)
        ttk.Entry(self.frame, width=10, textvariable=self.sid).grid(column=2,row=5 ,padx=8,pady=8)
        #Button connection
        ttk.Button(self.frame, text="Connexion", command=self.makeConnect).grid(column=2, row=6, padx=5, pady=5, sticky=E)

    #Le menu une fois connecter
    def menuPrincipal(self):
        self.clear()
        self.createFrame()
        ttk.Label(self.frame, text="Usager :" + " " +self.username.get()).grid(column=0, row=1, sticky=W)
        ttk.Label(self.frame, text="Serveur :" + " " + self.host.get()).grid(column=0, row=2, sticky=W)
        ttk.Label(self.frame, text="Port :" + " " + self.port.get()).grid(column=0, row=3, sticky=W)
        ttk.Label(self.frame, text="SID :" + " " + self.sid.get()).grid(column=0, row=4, sticky=W)
        ttk.Label(self.frame, text="Veuillez choisir un option :").grid(column=0, row=7, sticky=W)
        ttk.Button(self.frame, text="Requêtes", command=self.menuRequest).grid(column=0, row=9, sticky=W)
        ttk.Button(self.frame, text="Quitter", command=self.makeDisconnect).grid(column=5, row=12, sticky=W)



    #On créer la fenêtre principale
    def createFrame(self):
        self.frame = ttk.Frame(self.root, width=800,height=400)
        self.frame.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.columnconfigure(0, weight=1)
        self.frame.rowconfigure(0, weight=1)
    
    #Apelle la méthode callConnect dans le contrôleur et passe en paramètre nom, pass, host, port, sid
    def makeConnect(self):
        self.parent.callConnect(self.username.get(),self.password.get(),self.host.get(),self.port.get(),self.sid.get())

    def makeDisconnect(self):
        self.parent.disconnect()
        exit(0)

    #Efface le frame   
    def clear(self):
        self.frame.destroy()
    
    #Affiche le menu pour envoyer les requêtes
    def menuRequest(self):
        self.clear()
        self.createFrame()
        ttk.Button(self.frame, text="Retour Menu principale", command=self.menuPrincipal).grid(column=0, row=0, sticky=W)
        ttk.Label(self.frame, text="Entrer la requête SQL: " ).grid(column=0, row=1, sticky=W)
        ttk.Entry(self.frame, width=100, textvariable=self.request).grid(column=0,row=2,padx=8,pady=8)
        ttk.Button(self.frame, text="Envoyer", command=self.callRequest).grid(column=2, row=6, padx=5, pady=5, sticky=E)
        ttk.Button(self.frame, text="Effacer", command=self.effaceText).grid(column=0, row=6, padx=5, pady=5, sticky=W)
        self.text = Text(self.frame, width=70, height=20, state='normal')
        self.text.grid(column=0, row=3, sticky=E)
      
     #Permet d'afficher le retour de la requêtes dans la fenêtres   
    def insertResult(self, resultat):
        self.text.insert(INSERT,resultat)
        self.text.config(state='disabled')

    def effaceText(self):
        self.text = Text(self.frame, width=70, height=20, state='normal')
        self.text.grid(column=0, row=3, sticky=E)
     
     #Permet d'envoyer la requête au contrôleur qui l'envoi ensuite au modèle   
    def callRequest(self):
        self.text.config(state='normal')
        self.parent.request(self.request.get())

    #Démarre l'application
    def start(self):
        self.root.mainloop()

    #Affiche le message box si erreur    
    def messageBox(self, e):
        messagebox.showerror("Error", e)
    
        
        
        
        