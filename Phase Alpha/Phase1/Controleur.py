'''
Auteur: David Lebrun
Titre: Phase 1 - Projet Oracle
Remise: 19 mars 2013
'''
# -*- coding: utf-8 -*-
from Modele import *
from Vue import *

class Controleur:
    
    def __init__(self):
        self.modele = Modele(self)
        self.vue = Vue(self)
        self.vue.start() 
        
    #Apelle la méthode connexion dans la vue pour permettre la connexion à la base de données
    def callConnect(self, username, password, host, port, sid):
        self.modele.connexion(username, password, host, port, sid)

    #On apelle cette méthode si la connexion est valide
    def valideConnect(self):
        self.vue.clear()
        self.vue.menuPrincipal()
    
    #On envoi la requête au serveur de la base de données
    def request(self, request):
        self.modele.enTete(request)
    
    #Affiche un messagebox si erreur
    def messageError(self, e):
        self.vue.messageBox(e)
    
    #Retourne le résultat de la requête
    def getResult(self, resultat):
        self.vue.insertResult(resultat)

    #Permet la déconnexion
    def disconnect(self):
        self.modele.deconnexion()


if __name__ == '__main__':
    c = Controleur()
    