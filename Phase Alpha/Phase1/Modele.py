'''
Auteur: David Lebrun
Titre: Phase 1 - Projet Oracle
Remise: 19 mars 2013
'''
# -*- coding: utf-8 -*-
P = 'C:/Oracle/client/product/11.2.0/client_1/BIN'
import sys
sys.path.append(P)
import cx_Oracle

class Modele:
    
    def __init__(self, parent):
        self.parent = parent

    #Effectue la connexion à la database    
    def connexion(self, username, password, host, port, sid):
        try:
            self.dsn_tns = cx_Oracle.makedsn(host,port,sid)
            self.chaineConnexion = username + '/' + password + '@' + self.dsn_tns
            self.connexion = cx_Oracle.connect(self.chaineConnexion)
            self.curseur = self.connexion.cursor()
            print('Connexion réussi')
            self.parent.valideConnect()
        except Exception as e:
            self.parent.messageError(e)    
    
    #Permet de déconnecter l'usager/client
    def deconnexion(self):
        self.connexion.close()
        print('Déconnexion réussi')

    #On prend la requête sql pour l'exécuter à la base de données
    def requestSelect(self, request):
        resultat = self.nomColonne
        try:
            self.curseur.execute(request)
            for column in self.curseur.fetchall():
                for i in range(self.nbrColumn):
                    resultat += str(column[i]) + '    '
                resultat += '\n'
            self.sendResult(resultat)
        except Exception as e:
            self.parent.messageError(e)

    #Permet d'afficher l'en-tête de la requête sql , Nom des tables....
    def enTete(self, request):
        try:
            self.nbrColumn = 0;
            space = ""
            nomColonne = ""
            resultat = ""
            self.curseur.execute(request)
            for column in self.curseur.description:
                nomColonne += str(column[0]) + '  |  '
                resultat += str(column[0])
                space += "==========="
                self.nbrColumn += 1
            self.nomColonne = nomColonne + '\n' + space + '\n'
            self.requestSelect(request) 
        except Exception as e:
            self.parent.messageError(e)
    
    #Envoi le résultat au contrôleur qui ensuite l'envoi à la vue
    def sendResult(self, resultat):
        self.parent.getResult(resultat)
