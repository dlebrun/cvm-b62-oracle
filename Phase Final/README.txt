                 ___________       .__  __   
  _______  ______\__    ___/_  _  _|__|/  |_ 
_/ ___\  \/ /     \|    |  \ \/ \/ /  \   __\
\  \___\   /  Y Y  \    |   \     /|  ||  |  
 \___  >\_/|__|_|  /____|    \/\_/ |__||__|  
     \/          \/              

Application d�velopper par : 
	David Lebrun 	: dave.lebr1@gmail.com
	Francis St-Onge : webmastrefr1@gmail.com
	Antoine Martin 	: tonymartin_inc@hotmail.com
	Jeff Massey 	: jeff.massey.jm@gmail.com

�tudiant du C�gep du Vieux Montr�al

===========================================================================
				README
===========================================================================
Derni�re mise � jour : 27 mai 2013 

*************************
IMPORTANT
*************************
Pour pouvoir utiliser l'application vous devez avoir pr�alablement une base 
de donn�es oracle avec les fichiers SQL fournit. Les scripts pour que la base 
de donn�e puisse bien fonctionner sont dans le dossier DB.

**************************
Information g�n�ral
**************************
Tout d'abord, le but est de cr�er une application dans le m�me type que le 
g�ant des r�seaux sociaux "Twitter". CVMtwit poss�de les fonctionnalit�s 
suivantes: Pouvoir s'inscrire comme nouveaux utilisateurs, pouvoir se connecter 
avec un identifiant unique (nom d'usager / mot de passe), afficher les statuts 
de nos amis sur notre pas d'accueil, modifier son profil, �crire son propre 
statuts, se d�sabonner du r�seau sociaux CVMtwit et se d�connecter de l'application. 
Il y a �galement l'option de pouvoir changer son image de profil ou tout simplement 
garder l'image par d�faut du CVMtwit. Pour pouvoir s'inscrire � CVMtwit il faut fournir 
obligatoirement les informations suivantes : nom d'usager, mot de passe, nom, pr�nom 
et un courriel. �videmment, il est obligatoire de s'identifier pour acc�der au 
r�seau social CVMtwit.



