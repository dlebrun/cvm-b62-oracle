/*
*	Database.sql
*	Projet Oracle
*
* Modification possible: ajout de la date d'inscription au twiter!!!******************
*/
DROP TABLE FRIENDS;
DROP TABLE MESSAGE;
DROP TABLE USERS;

DROP SEQUENCE SEQ_ID_FRIENDS;
DROP SEQUENCE SEQ_ID_MESSAGE;
DROP SEQUENCE SEQ_ID_USERS;

CREATE TABLE USERS (
	ID NUMBER,
	USERNAME VARCHAR2(15) UNIQUE NOT NULL,
	PASSWORD VARCHAR2(15) NOT NULL,
	FIRSTNAME VARCHAR2(20) NOT NULL,
	LASTNAME VARCHAR2(20) NOT NULL,
	DATEINSCRIPTION DATE NOT NULL,
	DATENAISSANCE DATE,
	GENRE VARCHAR2(10) NOT NULL,
	BIOGRAPHY VARCHAR2(200) NOT NULL,
	EMAIL VARCHAR2(70) UNIQUE NOT NULL,
	IMAGE BLOB,
	CONSTRAINT pk_id_users PRIMARY KEY(ID)
);

CREATE TABLE MESSAGE (
	ID NUMBER,
	CONTENT VARCHAR2(120) NOT NULL,
	DATEMESSAGE DATE NOT NULL,
	ID_SENDER NUMBER,
	CONSTRAINT pk_id_message PRIMARY KEY(ID),
	CONSTRAINT fk_id_sender FOREIGN KEY(ID_SENDER) REFERENCES USERS(ID) ON DELETE CASCADE 
);

CREATE TABLE FRIENDS (
	ID NUMBER,
	ID_USERS NUMBER,
	ID_FRIENDS NUMBER,
	CONSTRAINT pk_id_friends PRIMARY KEY(ID),
	CONSTRAINT fk_id_users FOREIGN KEY(ID_USERS) REFERENCES USERS(ID) ON DELETE CASCADE,
	CONSTRAINT fk_id_users_friends FOREIGN KEY(ID_FRIENDS) REFERENCES USERS(ID) ON DELETE CASCADE
);

CREATE SEQUENCE seq_id_users START WITH 0 INCREMENT BY 1 minvalue 0;
CREATE SEQUENCE seq_id_message START WITH 0 INCREMENT BY 1  minvalue 0;
CREATE SEQUENCE seq_id_friends START WITH 0 INCREMENT BY 1  minvalue 0;

CREATE INDEX idx_users_lastname ON USERS(LASTNAME);
CREATE INDEX idx_users_firstname ON USERS(FIRSTNAME);
CREATE INDEX idx_message_date ON MESSAGE(DATEMESSAGE);